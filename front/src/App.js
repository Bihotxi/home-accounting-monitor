import React from "react";
import Income from "./pages/Income";
import Savings from "./pages/Savings";
import Expenses from "./pages/Expenses";
import RecordsContainer from "./components/Shared/RecordsContainer";
import SignupPage from "./pages/SignupPage";
import LoginPage from "./pages/LoginPage";
import HomePage from "./pages/HomePage";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import PrivateRoute from "./components/PrivateRoute";
import { AuthProvider } from "./components/Context/AuthContext";
import Header from "./components/Header/Header";
import { ThemeProvider } from "@mui/material/styles";
import theme from "./AppTheme";
import CssBaseline from "@mui/material/CssBaseline";
import "./App.css";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <>
        <AuthProvider>
          <Router>
            <Header />

            <Routes>
              <Route
                path="/ingresos"
                element={
                  <PrivateRoute>
                    <RecordsContainer>
                      <Income />
                    </RecordsContainer>
                  </PrivateRoute>
                }
              />
              <Route
                path="/gastos"
                element={
                  <PrivateRoute>
                    <RecordsContainer>
                      <Expenses />
                    </RecordsContainer>
                  </PrivateRoute>
                }
              />
              <Route
                path="/ahorros"
                element={
                  <PrivateRoute>
                    <RecordsContainer>
                      <Savings />
                    </RecordsContainer>
                  </PrivateRoute>
                }
              />
              <Route path="/registro" element={<SignupPage />} />
              <Route path="/login" element={<LoginPage />} />
              <Route path="/" element={<HomePage />} />
            </Routes>
          </Router>
        </AuthProvider>
      </>
    </ThemeProvider>
  );
}

export default App;
