import { useState } from "react";
import { createRecord } from "../utils/recordUtils.js";
import { useAuth } from "../components/Context/AuthContext.js";

function useCreateRecord() {
  const [entryCreated, setEntryCreated] = useState(false);
  const { currentUser } = useAuth();

  const create = (year, month, valuesArray) => {
    createRecord(year, month, valuesArray, setEntryCreated, currentUser.id);
  };

  return { createRecord: create, entryCreated, setEntryCreated };
}

export default useCreateRecord;
