import { useState } from "react";
import { handleDataChange, resetData } from "../utils/formDataUtils";

export function useFormData(initialData = {}) {
  const [formData, setFormData] = useState(initialData);

  const handleChange = (e) => {
    const newFormData = handleDataChange(formData, e);
    setFormData(newFormData);
  };

  const resetFormData = () => {
    const newFormData = resetData(initialData);
    setFormData(newFormData);
  };

  return {
    formData,
    setFormData,
    handleChange,
    resetFormData,
  };
}
