import { useState } from "react";
import { updateRecordUtil } from "../utils/recordUtils.js";
import { useAuth } from "../components/Context/AuthContext.js";


function useUpdateRecord() {
  const [isUpdated, setIsUpdated] = useState(false);
  const { currentUser } = useAuth();

  const resetIsUpdated = () => {
    setIsUpdated(false);
  };

  const updateExistingRecord = (year, month, recordId, valuesArray) => {
    updateRecordUtil(year, month, recordId, valuesArray, setIsUpdated, currentUser.id);
  };

  return { isUpdated, updateExistingRecord, resetIsUpdated };
}

export default useUpdateRecord;
