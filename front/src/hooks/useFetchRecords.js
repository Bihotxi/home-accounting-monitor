import { useState } from "react";
import { fetchRecordsUtil } from "../utils/recordUtils.js";
import { useAuth } from "../components/Context/AuthContext.js";

function useFetchRecords(setHasSearched) {
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const { currentUser } = useAuth();

  const fetchRecords = (searchYear, searchMonth) => {
    if (!currentUser) return;
    fetchRecordsUtil(
      searchYear,
      searchMonth,
      setData,
      setIsLoading,
      setHasSearched,
      currentUser.id
    );
  };

  return { data, isLoading, fetchRecords };
}

export default useFetchRecords;
