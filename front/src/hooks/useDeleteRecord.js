import { useState } from "react";
import { deleteRecordUtil } from "../utils/recordUtils.js";

function useDeleteRecord() {
  const [isDeleted, setIsDeleted] = useState(false);
  const [entryDeleted, setEntryDeleted] = useState(false);
  const [hasDeleted, setHasDeleted] = useState(false);

  const deleteExistingRecord = (year, month, recordId) => {
    deleteRecordUtil(
      year,
      month,
      recordId,
      setIsDeleted,
      setEntryDeleted,
      setHasDeleted
    );
  };

  return {
    isDeleted,
    deleteExistingRecord,
    entryDeleted,
    setEntryDeleted,
    hasDeleted,
    setHasDeleted,
  };
}

export default useDeleteRecord;
