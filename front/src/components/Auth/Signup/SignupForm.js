import React, { useState } from "react";
import { toast, Toaster } from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import { TextField } from "@mui/material";
import { Button } from "@mui/material";

const SignupForm = ({ onCloseModal }) => {
  const [formData, setFormData] = useState({
    username: "",
    email: "",
    password: "",
  });
  const navigate = useNavigate();

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch("http://localhost:3001/auth/signup", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      });

      const data = await response.json();

      if (response.ok) {
        localStorage.setItem("token", data.token);
        toast.success("Registrado correctamente");

        setTimeout(() => {
          onCloseModal();
          navigate("/");
        }, 2000);
      } else if (response.status === 401) {
        console.error("Error al intentar registrarse 1:", data.message);
        toast.error("Error al intentar registrarse");
      } else {
        console.error("Error al intentar registrarse 2:", data.message);
        toast.error("Error al intentar registrarse");
      }
    } catch (error) {
      toast.error("error");
    }
  };

  return (
    <div>
      <Toaster />
      <form onSubmit={handleSubmit}>
        <TextField
          label="Nombre de Usuario"
          type="text"
          name="username"
          id="username"
          onChange={handleChange}
          required
          fullWidth
          margin="normal"
          sx={{
            marginBottom: 2,
          }}
        />

        <TextField
          label="Email"
          type="text"
          name="email"
          id="email"
          onChange={handleChange}
          required
          fullWidth
          margin="normal"
          sx={{
            marginBottom: 2,
          }}
        />

        <TextField
          label="Contraseña"
          type="password"
          name="password"
          id="password"
          onChange={handleChange}
          required
          fullWidth
          margin="normal"
          sx={{
            marginBottom: 2,
          }}
        />

        <Button
          type="submit"
          color="primary"
          variant="contained"
          fullWidth
          sx={{ mt: 2, py: 1 }}
        >
          Registrarse
        </Button>
      </form>
    </div>
  );
};

export default SignupForm;
