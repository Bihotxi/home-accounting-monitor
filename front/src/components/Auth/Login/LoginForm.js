import React, { useState } from "react";
import { toast, Toaster } from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../../Context/AuthContext";
import { TextField } from "@mui/material";
import { Button } from "@mui/material";

const initialFormData = {
  username: "",
  password: "",
};

const LoginForm = ({ onLoginSuccess }) => {
  const [formData, setFormData] = useState(initialFormData);
  const { login } = useAuth();
  const navigate = useNavigate();

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch("http://localhost:3001/auth/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      });

      const data = await response.json();

      if (response.ok) {
        localStorage.setItem("token", data.token);
        login(data.user);
        navigate("/ahorros");
        onLoginSuccess();
      } else if (response.status === 401) {
        console.error("Error al intentar acceder:", data.message);
        toast.error("Usuario o contraseña incorrecta");
      } else {
        console.error("Error al intentar acceder:", data.message);
        toast.error("Error al intentar acceder");
      }
    } catch (error) {
      console.error("Error en la solicitud:", error);
      toast.error("Error al intentar acceder");
    }
    setFormData(initialFormData);
  };

  return (
    <div>
      <Toaster />
      <form onSubmit={handleSubmit}>
        <TextField
          label="Nombre de Usuario"
          type="text"
          name="username"
          value={formData.username}
          onChange={handleChange}
          required
          fullWidth
          margin="normal"
          sx={{
            marginBottom: 2,
          }}
        />
        <TextField
          label="Contraseña"
          type="password"
          name="password"
          value={formData.password}
          onChange={handleChange}
          required
          fullWidth
          margin="normal"
          sx={{
            marginBottom: 2,
          }}
        />
        <Button
          type="submit"
          color="primary"
          variant="contained"
          fullWidth
          sx={{ mt: 2, py: 1 }}
        >
          Acceder
        </Button>
      </form>
    </div>
  );
};

export default LoginForm;
