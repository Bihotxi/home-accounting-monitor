import React, { useState, useEffect } from "react";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import InputLabel from "@mui/material/InputLabel";
import Button from "@mui/material/Button";
import { FormControl } from "@mui/material";
import "./SelectYearMonth.css";

function SelectYearMonth({ year, setYear, month, setMonth, onSearch }) {
  const [localYear, setLocalYear] = useState(year);
  const [localMonth, setLocalMonth] = useState(month);

  const currentYear = new Date().getFullYear();
  const years = Array.from({ length: 10 }, (_, i) => currentYear + i);

  const months = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
  ];

  useEffect(() => {
    setLocalYear(year);
    setLocalMonth(month);
  }, [year, month]);

  const handleSearchClick = () => {
    setYear(localYear);
    setMonth(localMonth);
    onSearch(localYear, localMonth);
  };

  return (
    <div className="date-inputs-container">
      <FormControl>
        <InputLabel id="year-select-label">Año</InputLabel>
        <Select
          labelId="year-select-label"
          id="year-select"
          value={localYear}
          label="Año"
          onChange={(e) => setLocalYear(e.target.value)}
        >
          {years.map((year, index) => (
            <MenuItem key={index} value={year}>
              {year}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <FormControl>
        <InputLabel id="month-select-label">Mes</InputLabel>
        <Select
          labelId="month-select-label"
          id="month-select"
          value={localMonth}
          label="Mes"
          onChange={(e) => setLocalMonth(e.target.value)}
        >
          {months.map((month, index) => (
            <MenuItem key={index} value={month}>
              {month}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <Button
        variant="contained"
        size="large"
        sx={{
          ":hover": {
            color: "#0a66c2",
          },         
        }}
        onClick={handleSearchClick}
      >
        Buscar
      </Button>
    </div>
  );
}

export default SelectYearMonth;
