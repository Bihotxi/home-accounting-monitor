import React from "react";
import CalculatorInputValue from "./CalculatorInputValue";
import TextField from "@mui/material/TextField";
import { Button } from "@mui/material";
import "./NewRecord.css";

function NewRecord({
  formData,
  handleChange,
  handleSubmit,
  handleUpdate,
  fieldNames,
  showUpdateButton,
  isEditing,
  setIsEditing,
  createNewEntry,
  inputsEnabled,
  handleCancel,
  handleDelete,
  handleEdit,
}) {
  const handleCalculate = (fieldName, amount, operation) => {
    const currentValue = parseFloat(formData[fieldName] || 0);
    const newValue =
      operation === "add"
        ? currentValue + amount
        : operation === "subtract"
        ? currentValue - amount
        : currentValue;
    handleChange({
      target: { name: fieldName, value: newValue.toString() },
    });
  };

  return (
    <div>
      <form
        className="textFields-container"
        onSubmit={isEditing ? handleUpdate : handleSubmit}
      >
        {fieldNames.map((name) => (
          <TextField
            key={name}
            id="standard-number"
            type="number"
            disabled={
              name === "AHORROS TOTALES" ||
              name === "GASTOS TOTALES" ||
              name === "INGRESOS TOTALES" ||
              name === "AHORROS MENSUALES" ||
              !inputsEnabled
            }
            InputLabelProps={{
              shrink: true,
            }}
            name={name}
            label={name}
            value={formData[name] || ""}
            onChange={handleChange}
          />
        ))}

        {createNewEntry && (
          <Button
            variant="contained"
            size="small"
            sx={{
              ":hover": {
                color: "#0a66c2",
              },
              fontSize: "11px",
            }}
            type="submit"
          >
            Enviar
          </Button>
        )}
      </form>
      {showUpdateButton && !createNewEntry && (
        <div className="buttons-container">
          {!isEditing ? (
            <Button
              variant="contained"
              size="small"
              sx={{
                ":hover": {
                  color: "#0a66c2",
                },
                fontSize: "11px",
              }}
              onClick={() => handleEdit()}
            >
              Actualizar
            </Button>
          ) : (
            <>
              <Button
                variant="contained"
                size="small"
                sx={{
                  ":hover": {
                    color: "#0a66c2",
                  },
                  fontSize: "11px",
                }}
                onClick={(e) => {
                  handleUpdate(e);
                  setIsEditing(false);
                }}
              >
                Guardar Cambios
              </Button>
              <Button
                variant="contained"
                size="small"
                sx={{
                  ":hover": {
                    color: "#0a66c2",
                  },
                  fontSize: "11px",
                }}
                onClick={handleCancel}
              >
                Cancelar
              </Button>
              <Button
                variant="contained"
                size="small"
                sx={{
                  ":hover": {
                    color: "#0a66c2",
                  },
                  fontSize: "11px",
                }}
                onClick={handleDelete}
              >
                Eliminar
              </Button>
            </>
          )}
        </div>
      )}
      {isEditing && (
        <CalculatorInputValue
          fieldNames={fieldNames.filter(
            (name) =>
              name !== "AHORROS TOTALES " &&
              name !== "GASTOS TOTALES " &&
              name !== "INGRESOS TOTALES "
          )}
          onCalculate={handleCalculate}
        />
      )}
    </div>
  );
}

export default NewRecord;
