import { Button } from "@mui/material";
import React, { useState } from "react";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import TextField from "@mui/material/TextField";
import "./CalculatorInputValue.css";

function Calculator({ fieldNames, onCalculate }) {
  const [selectedField, setSelectedField] = useState("");
  const [amount, setAmount] = useState("");
  const [operation, setOperation] = useState("add");

  const handleCalculate = () => {
    onCalculate(selectedField, parseFloat(amount), operation);
    setSelectedField("");
    setAmount("");
  };

  return (
    <div className="calculator-container">
      <Button
        variant="contained"
        size="large"
        sx={{
          ":hover": {
            color: "#0a66c2",
          },
        }}
        onClick={() => {
          setSelectedField(fieldNames[0]);
        }}
      >
        Calculadora
      </Button>
      {selectedField && (
        <div className="textfields-calculator">
          <div className="inputs-container">
            <Select
              value={selectedField}
              onChange={(e) => setSelectedField(e.target.value)}
            >
              {fieldNames.map((name) => (
                <MenuItem key={name} value={name}>
                  {name}
                </MenuItem>
              ))}
            </Select>
            <TextField
              id="standard-number"
              type="number"
              value={amount}
              onChange={(e) => setAmount(e.target.value)}
              placeholder="Cantidad"
            />
            <Select
              value={operation}
              onChange={(e) => setOperation(e.target.value)}
            >
              <MenuItem value="add">SUMAR</MenuItem>
              <MenuItem value="subtract">RESTAR</MenuItem>
            </Select>
          </div>

          <Button
            variant="contained"
            size="large"
            sx={{
              ":hover": {
                color: "#0a66c2",
              },
            }}
            onClick={handleCalculate}
          >
            Calcular
          </Button>
        </div>
      )}
    </div>
  );
}

export default Calculator;
