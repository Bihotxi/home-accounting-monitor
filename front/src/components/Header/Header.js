import React, { useState } from "react";
import { useAuth } from "../Context/AuthContext";
import { useNavigate, useLocation } from "react-router-dom";
import Typography from "@mui/material/Typography";
import { Button } from "@mui/material";
import Toolbar from "@mui/material/Toolbar";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from "@mui/material";
import LoginForm from "../Auth/Login/LoginForm";
import SignupForm from "../Auth/Signup/SignupForm";
import { Link } from "react-router-dom";
import "./Header.css";

function Header() {
  const { currentUser, logout } = useAuth();
  const navigate = useNavigate();
  const location = useLocation();

  const [isLoginDialogOpen, setIsLoginDialogOpen] = useState(false);
  const [showLoginForm, setShowLoginForm] = useState(true);

  const toggleForm = () => {
    setShowLoginForm(!showLoginForm);
  };

  const handleLoginSuccess = () => {
    setIsLoginDialogOpen(false);
  };

  const handleOpenLoginDialog = () => {
    setIsLoginDialogOpen(true);
    setShowLoginForm(true);
  };

  const handleCloseLoginDialog = () => {
    setIsLoginDialogOpen(false);
    setShowLoginForm(true);
  };

  const handleCloseSignupDialog = () => {
    setIsLoginDialogOpen(false);
  };

  const handleLogin = () => {
    navigate("/login");
  };

  const handleLogout = () => {
    logout();
    navigate("/");
  };

  const returnToHomePage = () => {
    if (location.pathname !== "/") {
      navigate("/");
    }
  };

  const buttonTheme = (onClickHandler, buttonText) => {
    return (
      <Button
        variant="contained"
        size="small"
        sx={{
          ":hover": {
            color: "#0a66c2",
          },
          fontSize: "11px",
        }}
        onClick={
          buttonText === "Iniciar sesión"
            ? handleOpenLoginDialog
            : onClickHandler
        }
      >
        {buttonText}
      </Button>
    );
  };

  return (
    <>
      <Toolbar className="header">
        <Typography variant="h1" onClick={returnToHomePage}>
          HOME ACCOUNTING MONITOR
        </Typography>
        <div>
          {currentUser ? (
            <div className="header__user-container">
              <span className="header__username">
                Bienvenido, {currentUser.username}
              </span>
              {buttonTheme(handleLogout, "Cerrar sesión")}
            </div>
          ) : (
            location.pathname !== "/login" &&
            location.pathname !== "/registro" &&
            buttonTheme(handleLogin, "Iniciar sesión")
          )}
        </div>
      </Toolbar>
      <Dialog
        open={isLoginDialogOpen}
        onClose={handleCloseLoginDialog}
        sx={{
          "& .MuiDialog-paper": {
            padding: "20px",
            borderRadius: "8px",
          },
        }}
      >
        <DialogTitle>
          {showLoginForm ? "Iniciar Sesión" : "Registrarse"}
        </DialogTitle>
        <DialogContent>
          {showLoginForm ? (
            <>
              <LoginForm onLoginSuccess={handleLoginSuccess} />
              <Typography sx={{ textAlign: "center", marginTop: 2 }}>
                ¿No tienes cuenta? Accede{" "}
                <Link onClick={toggleForm} style={{ cursor: "pointer" }}>
                  aquí
                </Link>{" "}
                para registrarte.
              </Typography>
            </>
          ) : (
            <SignupForm onCloseModal={handleCloseLoginDialog} />
          )}
        </DialogContent>
        <DialogActions>
          <Button
            sx={{ color: "primary.main" }}
            onClick={handleCloseSignupDialog}
          >
            Cerrar
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

export default Header;
