export const addRecord = async (year, month, valuesArray, userId) => {
  try {
    const response = await fetch("http://localhost:3001/records", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ year, month, value: valuesArray, userId }),
    });

    if (!response.ok) {
      console.error("Server response:", await response.text());
      throw new Error("Error al agregar el registro");
    }

    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error:", error);
    throw error;
  }
};

export const getRecords = async (year, month, userId) => {
  try {
    const response = await fetch(
      `http://localhost:3001/records/${userId}/${year}/${month}`
    );

    if (!response.ok) {
      if (response.status === 404) {
        return [];
      }
      throw new Error("Error al recuperar los registros");
    }

    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error:", error);
    throw error;
  }
};

export const updateRecord = async (
  year,
  month,
  recordId,
  valuesArray,
  userId
) => {
  try {
    const response = await fetch(
      `http://localhost:3001/records/${year}/${month}/${recordId}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ value: valuesArray, userId }),
      }
    );

    if (!response.ok) {
      throw new Error("Error al actualizar el registro");
    }

    return await response.json();
  } catch (error) {
    console.error("Error:", error);
    throw error;
  }
};

export const deleteRecord = async (year, month, recordId) => {
  try {
    const response = await fetch(
      `http://localhost:3001/records/${year}/${month}/${recordId}`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    if (!response.ok) {
      throw new Error("Error al eliminar el registro");
    }

    return await response.json();
  } catch (error) {
    console.error("Error:", error);
    throw error;
  }
};
