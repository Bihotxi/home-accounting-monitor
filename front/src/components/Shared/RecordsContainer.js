import React, { useState, useEffect } from "react";
import SelectYearMonth from "../SelectYearMonth/SelectYearMonth";
import useFetchRecords from "../../hooks/useFetchRecords";
import useCreateRecord from "../../hooks/useCreateRecord";
import useUpdateRecord from "../../hooks/useUpdateRecord";
import useDeleteRecord from "../../hooks/useDeleteRecord";
import { toast, Toaster } from "react-hot-toast";
import {
  handleSearchLogic,
  calculateFormData,
  calculateUpdatedData,
} from "../../utils/recordsContainerUtils";
import { useAuth } from "../Context/AuthContext";
import NavBar from "../NavBar/NavBar";
import Button from "@mui/material/Button";

import "./RecordsContainer.css";

function RecordsContainer({ children }) {
  const [year, setYear] = useState("");
  const [month, setMonth] = useState("");
  const [formData, setFormData] = useState({});
  const [recordId, setRecordId] = useState(null);
  const [hasData, setHasData] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [noDataFound, setNoDataFound] = useState(false);
  const [createNewEntry, setCreateNewEntry] = useState(false);
  const [originalData, setOriginalData] = useState({});
  const [hasSearched, setHasSearched] = useState(false);

  const showUpdateButton = hasData;
  const inputsEnabled = isEditing || createNewEntry;

  const { data, isLoading, fetchRecords } = useFetchRecords(setHasSearched);
  const { createRecord, entryCreated, setEntryCreated } = useCreateRecord();
  const { isUpdated, updateExistingRecord, resetIsUpdated } = useUpdateRecord();
  const {
    deleteExistingRecord,
    isDeleted,
    entryDeleted,
    setEntryDeleted,
    hasDeleted,
    setHasDeleted,
  } = useDeleteRecord();

  const { currentUser } = useAuth();

  const resetDateSearch = () => {
    setYear("");
    setMonth("");
  };

  useEffect(() => {
    if (data && data.length > 0) {
      setFormData(calculateFormData(data));
      setHasData(true);
      setNoDataFound(false);
      setRecordId(data[0]._id);
    } else if (data && data.length === 0) {
      setHasData(false);
      setNoDataFound(true);
    }
  }, [data]);

  useEffect(() => {
    if (entryCreated) {
      setHasData(true);
      setCreateNewEntry(false);
      fetchRecords(year, month);
      setEntryCreated(false);
    }
  }, [entryCreated, year, month, fetchRecords, setEntryCreated]);

  useEffect(() => {
    if (entryDeleted) {
      toast.success("Entrada eliminada");
      setFormData({});
      setHasData(false);
      resetDateSearch();

      const timer = setTimeout(() => {
        setEntryDeleted(false);
      }, 3000);
      return () => clearTimeout(timer);
    }
  }, [entryDeleted, setEntryDeleted]);

  useEffect(() => {
    if (isUpdated) {
      toast.success("Registro actualizado");
      fetchRecords(year, month);
      resetIsUpdated();
    }
  }, [isUpdated, fetchRecords, month, year, resetIsUpdated]);

  const handleSearch = handleSearchLogic(
    setHasSearched,
    setHasData,
    setNoDataFound,
    fetchRecords
  );

  const handleChange = (e, name, value) => {
    if (e && e.target) {
      name = e.target.name;
      value = e.target.value;
    }
    setFormData((prevData) => calculateUpdatedData(prevData, name, value));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const valuesArray = Object.keys(formData).map((key) => ({
      description: key,
      amount: formData[key],
    }));

    try {
      if (createNewEntry) {
        await createRecord(year, month, valuesArray, currentUser.id);
        toast.success("Entrada creada con éxito");
        setEntryCreated(true);
        setHasData(true);
      } else {
        await updateExistingRecord(year, month, recordId, valuesArray);
        toast.success("Registro actualizado con éxito");
        fetchRecords(year, month);
      }
    } catch (error) {
      console.error("Error al guardar la entrada:", error);
      toast.error("Error al guardar la entrada.");
    }
  };

  const handleUpdate = async (e) => {
    e.preventDefault();
    const valuesArray = Object.keys(formData).map((key) => ({
      description: key,
      amount: formData[key],
    }));

    try {
      await updateExistingRecord(year, month, recordId, valuesArray);

      fetchRecords(year, month);
      setIsEditing(false);
    } catch (error) {
      console.error("Error al actualizar la entrada:", error);
      toast.error("Error al actualizar la entrada.");
    }
  };

  const handleDelete = () => {
    if (!recordId) {
      toast.error("recordId no es válido:", recordId);
      return;
    }
    deleteExistingRecord(year, month, recordId, currentUser.id);
    fetchRecords(year, month);

    setFormData({});
    setHasData(false);
    setNoDataFound(true);
    setRecordId(null);
    setOriginalData({});
    setIsEditing(false);
    setCreateNewEntry(false);
    resetDateSearch();
  };

  const handleEdit = () => {
    setOriginalData({ ...formData });
    setIsEditing(true);
  };

  const handleCancel = () => {
    setFormData({ ...originalData });
    setIsEditing(false);
  };

  return (
    <div>
      <Toaster />
      <section className="date-search-container">
        <SelectYearMonth
          year={year}
          setYear={setYear}
          month={month}
          setMonth={setMonth}
          onSearch={handleSearch}
        />
      </section>
      <div className="tabs-form-container">
        {hasData && <NavBar />}

        <div>
          {isLoading}
          {!entryCreated &&
            noDataFound &&
            hasSearched &&
            !createNewEntry &&
            year &&
            month && (
              <Button
                variant="contained"
                size="small"
                sx={{
                  ":hover": {
                    color: "#0a66c2",
                  },
                  fontSize: "11px",
                }}
                onClick={() => {
                  setCreateNewEntry(true);
                  setFormData({});
                }}
              >
                Crear nueva entrada
              </Button>
            )}

          {createNewEntry && (
            <Button
              variant="contained"
              size="small"
              sx={{
                ":hover": {
                  color: "#0a66c2",
                },
                fontSize: "11px",
              }}
              onClick={() => {
                setCreateNewEntry(false);
                setFormData({});
                resetDateSearch();
              }}
            >
              Cancelar
            </Button>
          )}

          {!isLoading &&
            !entryDeleted &&
            !entryCreated &&
            year &&
            month &&
            (hasData || isEditing || createNewEntry) &&
            React.cloneElement(children, {
              year,
              month,
              setYear,
              setMonth,
              formData,
              setFormData,
              recordId,
              showUpdateButton,
              isEditing,
              isDeleted,
              setIsEditing,
              noDataFound,
              createNewEntry,
              setCreateNewEntry,
              handleCancel,
              entryDeleted,
              setEntryDeleted,
              hasDeleted,
              setHasDeleted,
              entryCreated,
              setEntryCreated,
              handleChange,
              handleSubmit,
              handleUpdate,
              handleDelete,
              inputsEnabled,
              handleEdit,
            })}
        </div>
      </div>
    </div>
  );
}

export default RecordsContainer;
