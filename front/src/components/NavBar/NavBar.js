import React from "react";
import { Link, useLocation } from "react-router-dom";
import { Tabs, Tab } from "@mui/material";
import "./NavBar.css"

function NavBar() {
  const location = useLocation();
  const currentPath = location.pathname;

  return (
    <Tabs
      value={currentPath}
      sx={{
        backgroundColor: "#f8f9fa",
        ".MuiTabs-indicator": { backgroundColor: "#0a66c2" },
      }}
    >
      <Tab label="Ingresos" value="/ingresos" component={Link} to="/ingresos" />
      <Tab label="Gastos" value="/gastos" component={Link} to="/gastos" />
      <Tab label="Ahorros" value="/ahorros" component={Link} to="/ahorros" />
    </Tabs>
  );
}

export default NavBar;
