import { createTheme } from "@mui/material/styles";

const theme = createTheme({
  palette: {
    primary: {
      main: "#0a66c2",
    },
    secondary: {
      main: "#BF616A",
    },
    background: {
      default: "#f8f9fa",
      paper: "#D8DEE9",
    },
    text: {
      primary: "#2E3440",
      secondary: "#434C5E",
    },
  },
  typography: {
    fontFamily: [
      "Open Sans",
      "Roboto",
      "Helvetica",
      "Arial",
      "sans-serif",
    ].join(","),

    h1: {
      fontSize: "2.5rem",

      "&:hover": {
        cursor: "pointer",
      },
    },
    h2: {
      fontSize: "1.7rem",
      color: "#333",
    },

    button: {
      textTransform: "none",
    },
  },
});

export default theme;
