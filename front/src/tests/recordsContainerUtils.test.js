import {
  calculateFormData,
  calculateUpdatedData,
  handleSearchLogic,
} from "../utils/recordsContainerUtils";

describe("recordsContainerUtils", () => {
  describe("calculateFormData", () => {
    it("should correctly calculate form data from input data", () => {
      const data = [
        {
          value: [
            { description: "NÓMINA", amount: 1000 },
            { description: "BIZUMS", amount: 200 },
          ],
        },
      ];

      const expectedFormData = {
        NÓMINA: 1000,
        BIZUMS: 200,
        "BANCO SABADELL": 0,
        "AHORROS MENSUALES": 0,
        "AHORROS TOTALES": 0,
      };

      expect(calculateFormData(data)).toEqual(expectedFormData);
    });
  });

  describe("calculateUpdatedData", () => {
    it("should correctly update and calculate data", () => {
      const prevData = {
        NÓMINA: 1000,
        BIZUMS: 200,
        "INGRESOS TOTALES": 1200,
        "BANCO SABADELL": 500,
        "GASTOS TOTALES": 300,
        "AHORROS TOTALES": 1400,
      };

      const name = "NÓMINA";
      const value = 1500;
      const expectedData = {
        NÓMINA: value,
        BIZUMS: prevData.BIZUMS,
        "INGRESOS TOTALES": value + prevData.BIZUMS,
        "BANCO SABADELL": prevData["BANCO SABADELL"],
        "GASTOS TOTALES": prevData["GASTOS TOTALES"],
        "AHORROS MENSUALES": value + prevData.BIZUMS - prevData["GASTOS TOTALES"],
        "AHORROS TOTALES": prevData["BANCO SABADELL"] + (value + prevData.BIZUMS - prevData["GASTOS TOTALES"]),
      };

      expect(calculateUpdatedData(prevData, name, value)).toEqual(expectedData);
    });
  });

  describe("handleSearchLogic", () => {
    it("should handle search logic correctly", () => {
      const setHasSearched = jest.fn();
      const setHasData = jest.fn();
      const setNoDataFound = jest.fn();
      const fetchRecords = jest.fn();
      const searchYear = 2023;
      const searchMonth = 10;

      const handleSearch = handleSearchLogic(
        setHasSearched,
        setHasData,
        setNoDataFound,
        fetchRecords
      );
      handleSearch(searchYear, searchMonth);

      expect(setHasSearched).toHaveBeenCalledWith(false);
      expect(setHasData).toHaveBeenCalledWith(false);
      expect(setNoDataFound).toHaveBeenCalledWith(false);
      expect(fetchRecords).toHaveBeenCalledWith(searchYear, searchMonth);
      expect(setHasSearched).toHaveBeenCalledWith(true);
    });
  });
});
