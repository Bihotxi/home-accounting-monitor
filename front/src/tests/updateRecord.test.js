import { updateRecordUtil } from "../utils/recordUtils";
import { updateRecord } from "../components/DatabaseOperations";
import { toast } from "react-hot-toast";

jest.mock("../components/DatabaseOperations");
jest.mock("react-hot-toast", () => ({
  toast: {
    error: jest.fn(),
  },
}));

describe("recordUtils", () => {
  describe("updateRecordUtil", () => {
    it("should call updateRecord and set isUpdated to true on success", async () => {
      const mockSetIsUpdated = jest.fn();
      const year = "2023";
      const month = "10";
      const recordId = "123";
      const valuesArray = [{ description: "Test", amount: 100 }];
      const userId = "user123";


      updateRecord.mockResolvedValueOnce();

      await updateRecordUtil(
        year,
        month,
        recordId,
        valuesArray,
        mockSetIsUpdated,
        userId
      );

      expect(updateRecord).toHaveBeenCalledWith(
        year,
        month,
        recordId,
        valuesArray,
        userId
      );
      expect(mockSetIsUpdated).toHaveBeenCalledWith(true);
    });

    it("should call toast.error on failure", async () => {
      const mockSetIsUpdated = jest.fn();
      const year = "2023";
      const month = "10";
      const recordId = "123";
      const valuesArray = [{ description: "Test", amount: 100 }];
      const error = new Error("An error occurred");
      const userId = "user123";


      updateRecord.mockRejectedValueOnce(error);

      await updateRecordUtil(
        year,
        month,
        recordId,
        valuesArray,
        mockSetIsUpdated,
        userId
      );

      expect(updateRecord).toHaveBeenCalledWith(
        year,
        month,
        recordId,
        valuesArray,
        userId
      );
      expect(toast.error).toHaveBeenCalledWith(
        "Error al actualizar el registro:",
        error
      );
    });
  });
});
