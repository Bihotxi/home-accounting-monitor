import { deleteRecordUtil } from "../utils/recordUtils";
import { deleteRecord } from "../components/DatabaseOperations";

jest.mock("../components/DatabaseOperations");

describe("recordUtils", () => {
  describe("deleteRecordUtil", () => {
    it("should call deleteRecord and set flags on success", async () => {
      const mockSetIsDeleted = jest.fn();
      const mockSetEntryDeleted = jest.fn();
      const mockSetHasDeleted = jest.fn();
      const year = "2023";
      const month = "10";
      const recordId = "123";
      const userId = "user123";


      deleteRecord.mockResolvedValueOnce();

      await deleteRecordUtil(
        year,
        month,
        recordId,
        mockSetIsDeleted,
        mockSetEntryDeleted,
        mockSetHasDeleted,
        userId
      );

      expect(deleteRecord).toHaveBeenCalledWith(year, month, recordId, userId);
      expect(mockSetIsDeleted).toHaveBeenCalledWith(true);
      expect(mockSetEntryDeleted).toHaveBeenCalledWith(true);
      expect(mockSetHasDeleted).toHaveBeenCalledWith(true);
    });

    it("should log error on failure", async () => {
      const mockSetIsDeleted = jest.fn();
      const mockSetEntryDeleted = jest.fn();
      const mockSetHasDeleted = jest.fn();
      const year = "2023";
      const month = "10";
      const recordId = "123";
      const error = new Error("An error occurred");
      const userId = "user123";


      deleteRecord.mockRejectedValueOnce(error);

      console.error = jest.fn();

      await deleteRecordUtil(
        year,
        month,
        recordId,
        mockSetIsDeleted,
        mockSetEntryDeleted,
        mockSetHasDeleted,
        userId,
      );

      expect(deleteRecord).toHaveBeenCalledWith(year, month, recordId, userId);
      expect(console.error).toHaveBeenCalledWith(
        "Error al eliminar el registro:",
        error
      );
    });
  });
});
