import * as DatabaseOperations from "../components/DatabaseOperations";

describe("DatabaseOperations", () => {
  beforeEach(() => {
    global.fetch = jest.fn();
  });

  test("addRecord should make a POST request with correct data", async () => {
    const mockData = { message: "Record added" };
    fetch.mockResolvedValueOnce({
      ok: true,
      json: async () => mockData,
    });

    const response = await DatabaseOperations.addRecord("2023", "Enero", []);
    expect(response).toEqual(mockData);
    expect(fetch).toHaveBeenCalledWith("http://localhost:3001/records", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        year: "2023",
        month: "Enero",
        value: [],
      }),
    });
  });

  test("getRecords should make a GET request and return data", async () => {
    const mockData = [{ message: "Some records" }];
    const userId = "user123";

    fetch.mockResolvedValueOnce({
      ok: true,
      json: async () => mockData,
    });

    const response = await DatabaseOperations.getRecords(
      "2023",
      "Enero",
      userId
    );
    expect(response).toEqual(mockData);
    expect(fetch).toHaveBeenCalledWith(
      `http://localhost:3001/records/${userId}/2023/Enero`
    );
  });

  test("updateRecord should make a PUT request with correct data", async () => {
    const mockData = { message: "Record updated" };
    fetch.mockResolvedValueOnce({
      ok: true,
      json: async () => mockData,
    });

    const recordId = "someRecordId";
    const response = await DatabaseOperations.updateRecord(
      "2023",
      "Enero",
      recordId,
      []
    );
    expect(response).toEqual(mockData);
    expect(fetch).toHaveBeenCalledWith(
      `http://localhost:3001/records/2023/Enero/${recordId}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          value: [],
        }),
      }
    );
  });

  test("deleteRecord should make a DELETE request", async () => {
    const mockData = { message: "Record deleted" };
    const recordId = "someRecordId";

    fetch.mockResolvedValueOnce({
      ok: true,
      json: async () => mockData,
    });

    const response = await DatabaseOperations.deleteRecord(
      "2023",
      "Enero",
      recordId
    );
    expect(response).toEqual(mockData);
    expect(fetch).toHaveBeenCalledWith(
      `http://localhost:3001/records/2023/Enero/${recordId}`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
  });
});
