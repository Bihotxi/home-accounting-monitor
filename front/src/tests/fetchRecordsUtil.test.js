import { fetchRecordsUtil } from "../utils/recordUtils";
import { getRecords } from "../components/DatabaseOperations";
import { toast } from "react-hot-toast";

jest.mock("../components/DatabaseOperations");
jest.mock("react-hot-toast", () => ({
  toast: {
    error: jest.fn(),
  },
}));

describe("recordUtils", () => {
  describe("fetchRecordsUtil", () => {
    it("should fetch records and set data on success", async () => {
      const mockSetData = jest.fn();
      const mockSetIsLoading = jest.fn();
      const mockSetHasSearched = jest.fn();
      const year = "2023";
      const month = "10";
      const mockData = [{ description: "Test", amount: 100 }];
      const userId = "user123";

      getRecords.mockResolvedValueOnce(mockData);

      await fetchRecordsUtil(
        year,
        month,
        mockSetData,
        mockSetIsLoading,
        mockSetHasSearched,
        userId
      );

      expect(getRecords).toHaveBeenCalledWith(year, month, userId);
      expect(mockSetData).toHaveBeenCalledWith(mockData);
      expect(mockSetIsLoading).toHaveBeenCalledWith(false);
      expect(mockSetHasSearched).toHaveBeenCalledWith(true);
    });

    it("should call toast.error on failure", async () => {
      const mockSetData = jest.fn();
      const mockSetIsLoading = jest.fn();
      const mockSetHasSearched = jest.fn();
      const year = "2023";
      const month = "10";
      const error = new Error("An error occurred");
      const userId = "user123";

      getRecords.mockRejectedValueOnce(error);

      await fetchRecordsUtil(
        year,
        month,
        mockSetData,
        mockSetIsLoading,
        mockSetHasSearched,
        userId,
      );

      expect(getRecords).toHaveBeenCalledWith(year, month, userId);
      expect(toast.error).toHaveBeenCalledWith(
        "Error al recuperar registros:",
        error
      );
      expect(mockSetIsLoading).toHaveBeenCalledWith(false);
    });
  });
});
