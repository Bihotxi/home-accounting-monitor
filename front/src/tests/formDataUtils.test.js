import { handleDataChange, resetData } from "../utils/formDataUtils";

describe("formDataUtils", () => {
  it("should handle data changes", () => {
    const initialData = { initialField: "initialValue" };
    const mockEvent = { target: { name: "initialField", value: "newValue" } };

    const result = handleDataChange(initialData, mockEvent);

    expect(result).toEqual({
      initialField: "newValue",
    });
  });

  it("should reset data", () => {
    const initialData = { initialField: "initialValue" };

    const result = resetData(initialData);

    expect(result).toEqual(initialData);
  });
});
