import { createRecord } from "../utils/recordUtils";
import { addRecord } from "../components/DatabaseOperations";
import { toast } from "react-hot-toast";

jest.mock("../components/DatabaseOperations");
jest.mock("react-hot-toast", () => ({
  toast: {
    error: jest.fn(),
    success: jest.fn(),
  },
}));

describe("recordUtils", () => {
  describe("createRecord", () => {
    it("should call addRecord and set entryCreated to true on success", async () => {
      const mockSetEntryCreated = jest.fn();
      const year = "2023";
      const month = "10";
      const valuesArray = [{ description: "Test", amount: 100 }];
      const userId = "user123";

      addRecord.mockResolvedValueOnce();

      await createRecord(year, month, valuesArray, mockSetEntryCreated, userId);

      expect(addRecord).toHaveBeenCalledWith(year, month, valuesArray, userId);
      expect(mockSetEntryCreated).toHaveBeenCalledWith(true);
    });

    it("should call toast.error on failure", async () => {
      const mockSetEntryCreated = jest.fn();
      const year = "2023";
      const month = "10";
      const valuesArray = [{ description: "Test", amount: 100 }];
      const userId = "user123";
      const error = new Error("An error occurred");

      addRecord.mockRejectedValueOnce(error);

      await createRecord(year, month, valuesArray, mockSetEntryCreated, userId);

      expect(addRecord).toHaveBeenCalledWith(year, month, valuesArray, userId);
      expect(toast.error).toHaveBeenCalledWith(
        "Error al crear el registro:",
        error
      );
    });
  });
});
