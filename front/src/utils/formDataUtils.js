export function handleDataChange(formData, e) {
  const { name, value } = e.target;
  return {
    ...formData,
    [name]: value,
  };
}

export function resetData(initialData) {
  return initialData;
}
