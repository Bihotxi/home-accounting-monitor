import {
  addRecord,
  getRecords,
  deleteRecord,
  updateRecord,
} from "../components/DatabaseOperations";
import { toast } from "react-hot-toast";

export const createRecord = async (
  year,
  month,
  valuesArray,
  setEntryCreated,
  userId
) => {
  try {
    await addRecord(year, month, valuesArray, userId);
    setEntryCreated(true);
  } catch (error) {
    toast.error("Error al crear el registro:", error);
  }
};

export const fetchRecordsUtil = async (
  year,
  month,
  setData,
  setIsLoading,
  setHasSearched,
  userId
) => {
  setIsLoading(true);

  try {
    const fetchedData = await getRecords(year, month, userId);
    setData(fetchedData);
    setIsLoading(false);
  } catch (error) {
    toast.error("Error al recuperar registros:", error);
  } finally {
    setIsLoading(false);
    setHasSearched(true);
  }
};

export const deleteRecordUtil = async (
  year,
  month,
  recordId,
  setIsDeleted,
  setEntryDeleted,
  setHasDeleted,
  userId
) => {
  try {
    await deleteRecord(year, month, recordId, userId);
    setIsDeleted(true);
    setEntryDeleted(true);
    setHasDeleted(true);
  } catch (error) {
    console.error("Error al eliminar el registro:", error);
  }
};

export const updateRecordUtil = async (
  year,
  month,
  recordId,
  valuesArray,
  setIsUpdated,
  userId
) => {
  try {
    await updateRecord(year, month, recordId, valuesArray, userId);
    setIsUpdated(true);
  } catch (error) {
    toast.error("Error al actualizar el registro:", error);
  }
};
