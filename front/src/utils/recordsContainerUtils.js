export const calculateFormData = (data) => {
  const newFormData = {};
  data[0].value.forEach((record) => {
    newFormData[record.description] = record.amount;
  });

  if (newFormData["BANCO SABADELL"] === undefined) {
    newFormData["BANCO SABADELL"] = 0;
  }
  newFormData["AHORROS MENSUALES"] =
    (newFormData["INGRESOS TOTALES"] || 0) -
    (newFormData["GASTOS TOTALES"] || 0);

  newFormData["AHORROS TOTALES"] =
    (newFormData["INGRESOS TOTALES"] || 0) +
    (newFormData["BANCO SABADELL"] || 0) -
    (newFormData["GASTOS TOTALES"] || 0);

  return newFormData;
};

export const calculateUpdatedData = (prevData, name, value) => {
  const updatedData = { ...prevData, [name]: parseFloat(value) };

  const incomingsFields = ["NÓMINA", "BIZUMS", "PAGA EXTRA", "OTROS INGRESOS"];
  const savingsFields = ["INGRESOS TOTALES", "BANCO SABADELL"];
  const expensesFields = [
    "HIPOTECA",
    "COMUNIDAD",
    "INTERNET",
    "GASOLINA SEAT",
    "GASOLINA LAGUNA",
    "PLATAFORMAS V.O.D",
    "CLASES DE INGLÉS",
    "CLASES DE DIBUJO",
    "COMIDA (MENUDIET)",
    "GIMNASIO",
    "MERCADONA",
    "COMPRA DIARIA DEL PAN (TARJETA)",
    "PEDIDOS COMIDA A DOMICILIO",
    "COMEDOR DANIELA",
    "TLF. MÓVILES",
    "GAS",
    "LUZ",
    "RECIBO XBOX",
    "AGUA",
    "GASTOS VARIOS",
    "COMPRAS",
    "OCIO",
    "CAJERO",
    "AHORROS SABADELL",
    "AHORROS PARTICULARES",
  ];

  if (incomingsFields.includes(name)) {
    const totalIncomings = incomingsFields.reduce(
      (sum, field) => sum + parseFloat(updatedData[field] || 0),
      0
    );
    updatedData["INGRESOS TOTALES"] = totalIncomings;
  }

  if (expensesFields.includes(name)) {
    const totalExpenses = expensesFields.reduce(
      (sum, field) => sum + parseFloat(updatedData[field] || 0),
      0
    );
    updatedData["GASTOS TOTALES"] = totalExpenses;
  }

  const totalSavings = savingsFields.reduce(
    (sum, field) => sum + parseFloat(updatedData[field] || 0),
    0
  );

  updatedData["AHORROS TOTALES"] =
    totalSavings - (updatedData["GASTOS TOTALES"] || 0);

  updatedData["AHORROS MENSUALES"] =
    updatedData["INGRESOS TOTALES"] - updatedData["GASTOS TOTALES"];

  return updatedData;
};

export const handleSearchLogic =
  (setHasSearched, setHasData, setNoDataFound, fetchRecords) =>
  (searchYear, searchMonth) => {
    setHasSearched(false);
    setHasData(false);
    setNoDataFound(false);
    fetchRecords(searchYear, searchMonth);
    setHasSearched(true);
  };
