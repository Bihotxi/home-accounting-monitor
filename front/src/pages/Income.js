import React from "react";
import NewRecord from "../components/Records/NewRecord";

function Income(props) {
  const incomeNames = [
    "NÓMINA",
    "BIZUMS",
    "PAGA EXTRA",
    "OTROS INGRESOS",
    "INGRESOS TOTALES",
  ];

  return (
    <>
      <NewRecord {...props} fieldNames={incomeNames} />
    </>
  );
}
export default Income;
