import React from "react";
import { Link } from "react-router-dom";
import LoginForm from "../components/Auth/Login/LoginForm";
import { useNavigate } from "react-router-dom";

const LoginPage = () => {
  const navigate = useNavigate();

  const handleLoginSuccess = () => {
    navigate("/ahorros");
  };
  return (
    <div>
      <h2>Login</h2>

      <LoginForm onLoginSuccess={handleLoginSuccess} />
      <p>
        ¿No tienes cuenta?. Accede <Link to="/registro">aquí</Link> para
        registrarte.
      </p>
    </div>
  );
};

export default LoginPage;
