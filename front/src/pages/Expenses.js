import React from "react";
import NewRecord from "../components/Records/NewRecord";

function Expenses(props) {
  const expensesNames = [
    "HIPOTECA",
    "COMUNIDAD",
    "INTERNET",
    "GASOLINA SEAT",
    "GASOLINA LAGUNA",
    "PLATAFORMAS V.O.D",
    "CLASES DE INGLÉS",
    "CLASES DE DIBUJO",
    "COMIDA (MENUDIET)",
    "GIMNASIO",
    "MERCADONA",
    "COMPRA DIARIA DEL PAN (TARJETA)",
    "PEDIDOS COMIDA A DOMICILIO",
    "COMEDOR DANIELA",
    "TLF. MÓVILES",
    "GAS",
    "LUZ",
    "RECIBO XBOX",
    "AGUA",
    "GASTOS VARIOS",
    "COMPRAS",
    "OCIO",
    "CAJERO",
    "AHORROS SABADELL",
    "AHORROS PARTICULARES",
    "GASTOS TOTALES",
  ];

  return (
    <>
      <NewRecord {...props} fieldNames={expensesNames} />
    </>
  );
}

export default Expenses;
