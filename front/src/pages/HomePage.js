import React, { useState } from "react";
import {
  Typography,
  Container,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
} from "@mui/material";
import "./HomePage.css";

const HomePage = () => {
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Container maxWidth="sm" className="home-container">
      <div className="content-wrapper">
        <Typography variant="h2">
          Bienvenido a Home Accounting Monitor
        </Typography>
        <Typography variant="body1" paragraph>
          La forma sencilla de gestionar tus finanzas personales.
        </Typography>
      </div>
      <Button
        className="learn-more-button"
        variant="contained"
        color="primary"
        onClick={handleOpen}
        size="medium"
      >
        Aprende más
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        PaperProps={{
          className: "dialog-paper",
        }}
      >
        <DialogTitle className="dialog-title">
          Información Adicional
        </DialogTitle>
        <DialogContent className="dialog-content">
          <Typography variant="body1" gutterBottom>
            Home Accounting Monitor te ayuda a llevar un control claro y
            sencillo de tus finanzas personales. Con nuestra aplicación, puedes:
          </Typography>
          <ul>
            <li>
              <Typography variant="body2">
                Registrar y categorizar tus ingresos y gastos diarios para una
                mejor visibilidad de tu economía.
              </Typography>
            </li>
            <li>
              <Typography variant="body2">
                Establecer y seguir objetivos de ahorro, ayudándote a alcanzar
                tus metas financieras.
              </Typography>
            </li>
            <li>
              <Typography variant="body2">
                Visualizar tus hábitos de gasto a lo largo del tiempo con
                informes detallados y gráficos.
              </Typography>
            </li>
            <li>
              <Typography variant="body2" gutterBottom>
                Planificar tu presupuesto mensual y anual de manera efectiva.
              </Typography>
            </li>
          </ul>
          <Typography variant="body1">
            Comienza a organizar tus finanzas personales hoy mismo y toma el
            control de tu futuro económico.
          </Typography>
        </DialogContent>
      </Dialog>
    </Container>
  );
};

export default HomePage;
