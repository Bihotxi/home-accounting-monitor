import React from "react";
import NewRecord from "../components/Records/NewRecord";

function Savings(props) {
  const savingsNames = ["BANCO SABADELL", "AHORROS TOTALES", "AHORROS MENSUALES"];

  return (
    <>
      <NewRecord {...props} fieldNames={savingsNames} />
    </>
  );
}
export default Savings;
