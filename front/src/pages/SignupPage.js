import React from "react";
import { Link } from "react-router-dom";
import SignupForm from "../components/Auth/Signup/SignupForm";

const SignupPage = () => {
  return (
    <div>
      <h2>Registro</h2>

      <SignupForm />
      <p>
        ¿Ya tienes cuenta?. Inicia sesión <Link to="/login">aquí</Link>.
      </p>
    </div>
  );
};

export default SignupPage;
