const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
require("dotenv").config({ path: ".env.local" });
const bcrypt = require("bcryptjs");
const User = require("./models/userSchema");

const {
  DB_USER,
  DB_PASSWORD,
  DB_HOST,
  DB_PORT,
  DB_NAME,
  DB_ADMIN,
  DB_EMAIL,
  ADMIN_PASSWORD,
} = process.env;
const dbURL = `mongodb://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}`;

const app = express();
const port = 3001;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

mongoose
  .connect(dbURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(async () => {
    console.log("Conectado a MongoDB");

    const adminExists = await User.findOne({ username: DB_ADMIN });

    if (!adminExists) {
      const adminUser = new User({
        username: DB_ADMIN,
        email: DB_EMAIL,
        passwordHash: await bcrypt.hash(ADMIN_PASSWORD, 10),
        role: "admin",
      });
      await adminUser.save();
      console.log("Usuario admin creado exitosamente");
    }
  })
  .catch((error) => {
    console.error("Error al conectar a MongoDB:", error.message);
  });

app.use("/auth", require("./routes/authRoutes"));
app.use("/", require("./routes/routes"));

app.listen(port, () => {
  console.log(`Servidor escuchando en el puerto ${port}`);
});
