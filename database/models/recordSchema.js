const mongoose = require("mongoose");

const expenseSchema = new mongoose.Schema({
  description: String,
  amount: Number,
});

const recordSchema = new mongoose.Schema({
  userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  year: Number,
  month: String,
  value: [expenseSchema],
});

const Record = mongoose.model("Record", recordSchema);

module.exports = Record;
