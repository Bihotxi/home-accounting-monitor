const express = require("express");
const router = express.Router();
const Record = require("../models/recordSchema");

router.post("/records", async (req, res) => {
  console.log("POST request to /records received", req.body);
  try {
    const newRecord = new Record({ ...req.body, userId: req.body.userId });
    console.log("New record to be saved:", newRecord);
    await newRecord.save();
    res.status(201).json(newRecord);
  } catch (error) {
    console.error("Error saving record:", error);
    res.status(500).json({ message: error.message });
  }
});

router.get("/records/:id/:year/:month", async (req, res) => {
  try {
    const { id, year, month } = req.params;
    const yearInt = parseInt(year);
    const records = await Record.find({ userId: id, year: yearInt, month });

    if (records.length === 0) {
      return res.status(404).json({ message: "Registros no encontrados" });
    }
     res.status(200).json(records);
  } catch (error) {
    console.error("Error en la petición GET:", error);
    res.status(500).json({ message: error.message });
  }
});

router.put("/records/:year/:month/:id", async (req, res) => {
  try {
    const year = parseInt(req.params.year);
    const month = req.params.month;
    const recordId = req.params.id;
    const updatedData = req.body;

    const record = await Record.findOneAndUpdate(
      { year, month, _id: recordId },
      updatedData,
      { new: true }
    );

    if (!record) {
      return res.status(404).json({ message: "Registro no encontrado" });
    }

    res.status(200).json(record);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

router.delete("/records/:year/:month/:id", async (req, res) => {
  try {
    const year = parseInt(req.params.year);
    const month = req.params.month;
    const recordId = req.params.id;

    const record = await Record.findOneAndDelete({
      year,
      month,
      _id: recordId,
    });

    if (!record) {
      return res.status(404).json({ message: "Registro no encontrado" });
    }

    res.status(200).json({ message: "Registro eliminado exitosamente" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

module.exports = router;
