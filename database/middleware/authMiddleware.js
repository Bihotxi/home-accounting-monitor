const jwt = require("jsonwebtoken");
const User = require("../models/userSchema");

const isAdmin = async (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET_KEY);
    const user = await User.findById(decoded.userId);

    if (user.role !== "admin") {
      return res
        .status(403)
        .json({
          message: "Acceso denegado: No tienes permisos de administrador",
        });
    }

    next();
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = { isAdmin };
